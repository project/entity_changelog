<?php

/**
 * @file
 * Implement hooks for the entity_changelog module.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_changelog\Type\EntityChangelogOperation;
use Drupal\views\ViewExecutable;

/**
 * Log entity updates.
 *
 * @see \hook_entity_update()
 */
function entity_changelog_entity_update(EntityInterface $entity): void {
  /** @var \Drupal\entity_changelog\Services\EntityChangelogLogger $entity_changelog_logger */
  $entity_changelog_logger = Drupal::service(
    'entity_changelog.entity_changelog_logger'
  );
  $entity_changelog_logger->addEntry($entity, EntityChangelogOperation::UPDATE);
}

/**
 * Log entity insertions.
 *
 * @see \hook_entity_insert()
 */
function entity_changelog_entity_insert(EntityInterface $entity): void {
  /** @var \Drupal\entity_changelog\Services\EntityChangelogLogger $entity_changelog_logger */
  $entity_changelog_logger = Drupal::service(
    'entity_changelog.entity_changelog_logger'
  );
  $entity_changelog_logger->addEntry($entity, EntityChangelogOperation::INSERT);
}

/**
 * Log entity deletions.
 *
 * @see \hook_entity_delete()
 */
function entity_changelog_entity_delete(EntityInterface $entity): void {
  /** @var \Drupal\entity_changelog\Services\EntityChangelogLogger $entity_changelog_logger */
  $entity_changelog_logger = Drupal::service(
    'entity_changelog.entity_changelog_logger'
  );
  $entity_changelog_logger->addEntry($entity, EntityChangelogOperation::DELETE);
}

/**
 * Set groups for specific exposed filters of the entity changelog view.
 *
 * @see \hook_views_pre_build()
 */
function entity_changelog_views_pre_build(ViewExecutable $view): void {
  if ($view->id() !== 'entity_changelog') {
    return;
  }
  /** @var \Drupal\entity_changelog\Services\EntityChangelogLogger $entity_changelog_logger */
  $entity_changelog_logger = Drupal::service(
    'entity_changelog.entity_changelog_logger'
  );

  $logged_entity_types = [];
  foreach ($entity_changelog_logger->getLoggedEntityTypes() as $row) {
    $logged_entity_types[] = [
      'title' => $row->entity_type,
      'operator' => '=',
      'value' => $row->entity_type,
    ];
  }
  $view->filter["entity_type"]->options["group_info"]['group_items'] = $logged_entity_types;

  $logged_usernames = [];
  foreach ($entity_changelog_logger->getLoggedUsernames() as $row) {
    $logged_usernames[] = [
      'title' => $row->username,
      'operator' => '=',
      'value' => $row->username,
    ];
  }
  $view->filter["username"]->options["group_info"]['group_items'] = $logged_usernames;
}

/**
 * Run cron tasks for this module.
 *
 * @see \hook_cron()
 */
function entity_changelog_cron(): void {
  /** @var \Drupal\entity_changelog\Services\EntityChangelogLogger $entity_changelog_logger */
  $entity_changelog_logger = Drupal::service(
    'entity_changelog.entity_changelog_logger'
  );
  $entity_changelog_logger->deleteOldEntries();
}
