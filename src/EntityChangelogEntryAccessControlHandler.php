<?php

namespace Drupal\entity_changelog;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the entity changelog entity.
 *
 * @see \Drupal\entity_changelog\Entity\EntityChangelogEntry
 */
class EntityChangelogEntryAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(
    EntityInterface $entity,
    $operation,
    AccountInterface $account,
  ): AccessResultInterface {
    return AccessResult::forbiddenIf(
      !$account->hasPermission('access entity changelog'),
    );
  }

}
