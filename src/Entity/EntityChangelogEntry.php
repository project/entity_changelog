<?php

namespace Drupal\entity_changelog\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the entity changelog entry entity.
 *
 * @ContentEntityType(
 *   id = "entity_changelog_entry",
 *   label = @Translation("Entity Changelog Entry"),
 *   base_table = "entity_changelog_entry",
 *   handlers = {
 *      "view_builder" = "Drupal\entity_changelog\EntityChangelogEntryViewBuilder",
 *      "views_data" = "Drupal\entity_changelog\EntityChangelogEntryViewsData",
 *      "access" = "Drupal\entity_changelog\EntityChangelogEntryAccessControlHandler",
 *    },
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   },
 * )
 */
class EntityChangelogEntry extends ContentEntityBase {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['entity_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity Type'))
      ->setDescription(t('The type of the entity that was created, updated or deleted.'));

    $fields['entity_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity ID'))
      ->setDescription(t('The id of the entity that was created, updated or deleted.'));

    $fields['entity_title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity Title'))
      ->setDescription(t('The title of the entity that was created, updated or deleted.'));

    $fields['timestamp'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Timestamp of Operation'))
      ->setDescription(t('The timestamp of when the entity was created, updated or deleted.'));

    $fields['user_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('User ID'))
      ->setDescription(t('The ID of the user who created, updated or deleted the entity.'));

    $fields['username'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Username'))
      ->setDescription(t('The name of the user who created, updated or deleted the entity.'));

    $fields['request_path'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Request Path'))
      ->setDescription(t('The request path from which the creation, update or deletion was invoked.'));

    $fields['operation'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Operation'))
      ->setDescription(t('The type of the operation (create, update or delete)'));

    return $fields;
  }

}
