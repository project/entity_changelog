<?php

namespace Drupal\entity_changelog\Services;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\entity_changelog\Entity\EntityChangelogEntry;
use Drupal\entity_changelog\Type\EntityChangelogOperation;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Manager class of the entity changelog entries.
 */
class EntityChangelogLogger {

  use LoggerAwareTrait;

  /**
   * The entity changelog entry entity type.
   */
  private EntityTypeInterface $entityChangelogEntryType;

  /**
   * Constructor.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    LoggerInterface $logger,
    private AccountProxyInterface $currentUser,
    private RequestStack $requestStack,
    private Connection $connection,
  ) {
    $this->entityChangelogEntryType = $entity_type_manager->getDefinition(
      'entity_changelog_entry'
    );
    $this->setLogger($logger);
  }

  /**
   * Add an entity changelog entry to the log.
   */
  public function addEntry(
    EntityInterface $entity,
    EntityChangelogOperation $operation,
  ): void {
    if (!$entity->id() || $entity->getEntityTypeId() === 'entity_changelog_entry') {
      // Do not log entities without id or the changelog entry itself.
      return;
    }

    $data = [
      'entity_type' => $entity->getEntityType()->id() ?: NULL,
      "entity_id" => ((string) $entity->id()) ?: NULL,
      "entity_title" => $entity->label() ?: NULL,
      "timestamp" => (new \DateTime())->getTimestamp(),
      "user_id" => $this->currentUser->id() ?: NULL,
      "username" => $this->currentUser->getDisplayName() ?: NULL,
      "request_path" => $this->getRequestPathAndQuery(),
      "operation" => $operation->value,
    ];
    try {
      EntityChangelogEntry::create($data)->save();
    }
    catch (\Throwable $e) {
      $this->logger->error(
        "Could not create an entity changelog entry: {$e->getMessage()}",
        [
          'exception' => $e,
          'data' => $data,
        ],
          );
    }
  }

  /**
   * Delete entity changelog entries which are older than three years.
   */
  public function deleteOldEntries(): void {
    $three_years_ago = (new \DateTime())->sub(new \DateInterval('P3Y'))
      ->getTimestamp();
    $query = $this->connection->delete(
      $this->entityChangelogEntryType->getBaseTable(),
    );
    $query->condition('timestamp', $three_years_ago, '<');
    $query->execute();
  }

  /**
   * Get already logged entity types.
   *
   * @return \Traversable<object{entity_type: string}>
   *   the query result
   */
  public function getLoggedEntityTypes(): \Traversable {
    $query = $this->connection->select(
      $this->entityChangelogEntryType->getBaseTable(),
      'ece',
    )->distinct()->fields('ece', ['entity_type']);
    return $query->execute();
  }

  /**
   * Get already logged usernames.
   *
   * @return \Traversable<int, object{username: string}>
   *   the query result
   */
  public function getLoggedUsernames(): \Traversable {
    $query = $this->connection->select(
      $this->entityChangelogEntryType->getBaseTable(),
      'ece',
    )->distinct()->fields('ece', ['username']);
    return $query->execute();
  }

  /**
   * Get the request path with the query string.
   *
   * The request object itself does not offer such a method.
   * The logic comes from
   * {@see \Symfony\Component\HttpFoundation\Request::getUri}.
   */
  private function getRequestPathAndQuery(): ?string {
    $request = $this->requestStack->getCurrentRequest();
    if (!$request) {
      return NULL;
    }
    if (($qs = $request->getQueryString()) !== NULL) {
      $qs = '?' . $qs;
    }

    return $request->getPathInfo() . $qs;
  }

}
