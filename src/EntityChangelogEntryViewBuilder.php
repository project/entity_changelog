<?php

namespace Drupal\entity_changelog;

use Drupal\Core\Entity\EntityViewBuilder;

/**
 * View builder for entity changelog entries.
 */
class EntityChangelogEntryViewBuilder extends EntityViewBuilder {

}
