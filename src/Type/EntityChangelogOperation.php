<?php

/**
 * @file
 * Type of entity changelog operations.
 */

namespace Drupal\entity_changelog\Type;

enum EntityChangelogOperation: string {

  case INSERT = 'insert';

  case UPDATE = 'update';

  case DELETE = 'delete';

}
