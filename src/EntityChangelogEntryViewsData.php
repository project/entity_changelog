<?php

namespace Drupal\entity_changelog;

use Drupal\views\EntityViewsData;

/**
 * Views integration for entity changelog entries.
 */
class EntityChangelogEntryViewsData extends EntityViewsData {

}
