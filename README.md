# Entity Changelog

This module logs CUD (create, update, delete) operations on entities
and provides a view to look at them.

## Development Quickstart

Start docker containers:
```shell
docker compose up
```

Find out which host port was chosen:
```shell
docker compose port drupal 80
```

Open `http://localhost:<chosen_host_port>` and set up Drupal,
installing the module after.

Install composer development dependencies:
```shell
docker compose exec drupal composer install
```

## Contributing

### Drupal Code Style

Check for issues with the Drupal code style:
```shell
docker compose exec drupal vendor/bin/phpcs \
--standard=Drupal,DrupalPractice \
--extensions=php,module,inc,install,test,profile,theme,css,info,txt,md,yml \
--ignore=vendor \
.
```
